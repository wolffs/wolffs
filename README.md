# wolffs

I'm jadon and I work at [Yoco](https://www.yoco.com/)

# Currently working in

- Kotlin
- Python
- JavaScript/TypeScript
- Scala
- Golang

# Projects

- [phocus](https://github.com/wolffshots/phocus)
- [hass-audiobookshelf](https://github.com/wolffshots/hass-audiobookshelf)

# Personal work

- [GitHub](https://www.github.com/wolffshots)
- [GitLab](https://www.gitlab.com/wolffshots)
